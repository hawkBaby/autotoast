import unittest
import argparse
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
 
class autoToast(unittest.TestCase):
    
    def setUp(self):
        self.options = Options()
        self.options.add_argument("--headless")
        self.driver=webdriver.Firefox()
        
 
    def run_setup_func(self, opt):
        driver=self.driver
        driver.get("https://easy-speak.org")
        self.assertIn("Toastmasters - easy-Speak - Toastmaster Automation! :: Home",driver.title)
 
        #Select login details
        user = driver.find_element_by_css_selector("#username")
        user.send_keys("jad_p")
        password = driver.find_element_by_css_selector("body > table:nth-child(2) > tbody > tr > td:nth-child(1) > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td > span > input:nth-child(6)")
        password.send_keys("test30")
        driver.find_element_by_css_selector("body > table:nth-child(2) > tbody > tr > td:nth-child(1) > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td > input").click()
        driver.find_element_by_css_selector("body > table:nth-child(2) > tbody > tr > td:nth-child(1) > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td > a:nth-child(6)").click()
        page_title = driver.find_element_by_class_name("reversetitle")
        
        comms = driver.find_element_by_css_selector("#main-menu > li:nth-child(4) > a")

        hover = ActionChains(self.driver).move_to_element(comms)
        hover.perform()

        email_button = driver.find_element_by_css_selector(".postbody > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(3) > a:nth-child(3)")
        email_button.click()

        if opt == 1:
            first_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(8) > td:nth-child(2) > input:nth-child(2)")
            first_email.click()

        if opt == 2:
            second_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(12) > td:nth-child(2) > input:nth-child(2)")
            second_email.click()
        
        if opt == 3:
            third_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(17) > td:nth-child(2) > input:nth-child(2)")
            third_email.click()

        if opt == 4:
            fourth_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(21) > td:nth-child(2) > input:nth-child(2)")
            fourth_email.click()

        if opt == 5:
            fifth_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(25) > td:nth-child(2) > input:nth-child(2)")
            fifth_email.click()

        if opt == 6:
            mentor_email = driver.find_element_by_css_selector("table.forumline:nth-child(1) > tbody:nth-child(1) > tr:nth-child(28) > td:nth-child(2) > input:nth-child(2)")
            mentor_email.click()

        

    def testFirstEmail(self):
        self.run_setup_func(1)
    
    def testSecondEmail(self):
        self.run_setup_func(2)

    def testThirdEmail(self):
        self.run_setup_func(3)
    
    def testForthEmail(self):
        self.run_setup_func(4)
    
    def testFifthEmail(self):
        self.run_setup_func(5)

    def testMentorEmail(self):
        self.run_setup_func(6)
    

    def tearDown(self):
        self.driver.close()
 
if __name__=="__main__":    
    unittest.main()